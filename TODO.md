Hagrid Todo list
====

## Templates

#### HTML > Style/Markup
- [ ] templates/confirm-email-html.hbs
- [ ] templates/confirm.html.hbs 
- [ ] templates/delete.html.hbs
- [x] templates/index.html.hbs
- [x] templates/layout.html.hbs
- [ ] templates/upload.html.hbs
- [ ] templates/verify.html.hbs


#### EMail > Improve Text
- [ ] templates/confirm-email-txt.hbs
- [ ] templates/verify-email-txt.hbs

#### JS/SCSS > Webpack Code Splitting
* Reducing JS/CSS size
- [ ] templates/index.js
- [ ] templates/index.scss
